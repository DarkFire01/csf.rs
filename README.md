# Mali Command Stream Frontend

Replaces job manager.

Firmware blob runs on Arm Cortex-M <!-- TODO: which one --> processor running
in Thumb-2 mode. Based on the instructions used, per Wikipedia table of
Cortex-M, options are: M3, M4, M7, M33, M35P, M55. However the firmware uses
Private Peripheral Bus registers unavailable in M3 or M4. So it's an M7 or
newer.

* Does not use smc/svc
* Does not use coprocessor

The firmware is uploaded by the kernel.

Executes code starting from 0x1000000. The `parse-csf` utility in this tree
dumps the code segment to a file `code.bin`. After running, hypothetically
could run

	$ arm-linux-gnueabihf-objdump -D -bbinary -marm -Mforce-thumb code.bin --adjust-vma=0x1000000

## Responsibilities

* Protected mode handling
* Resource allocation
* Instruction emulation
* Creation/submission of jobs

[1] https://www.anandtech.com/show/16694/arm-announces-new-malig710-g610-g510-g310-mobile-gpu-families/2 

## Blob

Firmware blob named `mali_csffw.bin`. Not included in this repository for legal reasons, but can be found in the DDK.

### File format

Reconstructed from `mali_kbase_csf_firmware.c`:

	struct header {
		u32 magic = `0xC3F13A6E`
		u8 version_minor
		u8 version_major
		u16 padding
		u32 version_hash
		u32 padding
		u32 entry_end_offset
	};

After the header are entries until `entry_end_offset`

	struct entry_header {
		u8 type;
		u8 size; // including header
		u16 flags:
			(1 << 14) -- update
			(1 << 15) -- optional
	};

Each entry consists of `size` bytes. The interpretation depends on the value of type.

**Interface** records. Type = 0, used to specify mapped memory.

	struct interface {
		u32 flags;
		u32 virtual_start;
		u32 virtual_end;
		u32 data_start;
		u32 data_end;
	};

**Tracebuffer** records. Type = 3

	pub struct Tracebuffer {
	    pub type_: u32,
	    pub size_address: u32,
	    pub insert_address: u32,
	    pub extract_address: u32,
	    pub data_address: u32,
	    pub trace_enable: u32,
	    pub trace_enable_entry_count: u32,
	    pub name: String,
	}

# Tracebuffers

Tracebuffers are allocated by the main CPU for two-way communication between the CPU and the MCU. The firmware header specifies for MCU address (size, insert, extract, data, trace enable) within the read-only data section. The CPU is expected to write to these addresses:

* Size: The size of the data buffer
* Insert: Pointer to the insert offset
* Extract: Pointer to the extract offset
* Data: Pointer to the data buffer
* Trace enable: Bitmap of enabled tracepoints/features.

The firmware defines the following tracebuffers:

* fwlog: Firmware logs. 4 pages.
* benchmark: Benchmaring. 2 pages.
* timeline: Timeline trace points. 128 pages.
* fwutf: Firmware unit test framework. 1 page.
* firmware trace: Unused by kbase.

Firmware log is the most interesting to us.

# Memory map

Parsed from the firmware.

```
0x0000000 - 0x0001000: r-- Data
0x1000000 - 0x1010000: r-x Code
0x2000000 - 0x203e000: rw- RAM
0x203e000 - 0x203f000: rw- RAM
0x203f000 - 0x2040000: rw- RAM
0x2800000 - 0x2840000: r-- Zero
0x3000000 - 0x303e000: rw- Protected memory
0x303e000 - 0x303f000: rw- Protected memory
0x303f000 - 0x3040000: rw- Protected memory
0x4000000 - 0x4094000: rw- CPU/GPU shared buffer
```

All memory is cached. The CPU/GPU shared buffer is coherent; other memory is not.

# Debug

The firmware contains some debug `printf`s that use the semihosting interface

[https://www.keil.com/support/man/docs/armcc/armcc_pge1358787046598.htm](Arm semihosting interface)

That is, they do a `bkpt 0xab` instruction with `r0 = 0x3` and `r1` a pointer to a character of interest to implement a debug `putchar`. This can be used to retrieve a pretty-printed register dump when the hardware crashes, which is easy enough to induce from the emulator. And poof, registers.

Logically, the register dump is missing the _HI half of registers. But that makes sense -- this is a 32-bit MCU, so these halves should zero. For completeness the _HI halves are included in the following tables.

# Interrupts

The MCU is connected to a number of interrupts, expressed with the usual Cortex-M interrupt vector table. Known interrupts:

|IRQ#    | Meaning |
|------- | --------|
|0       | GPU     |
|1       | Doorbell/MCU |
|2       | Unused  |
|3       | Scoreboard all zero |
|4       | Scoreboard mask zero |
|5       | Scoreboard fault |
|6       | Timer0  |
|7       | Timer1  |
|8       | CSWHIF0 |
|9       | CSWHIF1 |
|10      | CSWHIF2 |
|11      | CSWHIF3 |
|12      | Compute iterator |
|13      | Fragment iterator |
|14      | Tiler iterator |

# Registers

## Control (`GPU_CONTROL_MCU`)

Base: 0x40003000

|Offset |  Name |
|------ |  ---- |
|0x20   | GPU_RAWSTAT |
|0x24   | GPU_IRQ_CLEAR |
|0x28   | GPU_IRQ_MASK |
|0x2c   | GPU_IRQ_STATUS |
|0x30   | GPU_COMMAND |
|0x34   | GPU_STATUS |
|0x3c   | GPU_FAULTSTATUS |
|0x40   | GPU_FAULTADDRESS_LO |
|0x44   | GPU_FAULTADDRESS_HI |

## MCU subsystem

Base: 0x40020000

|Offset |  Name                      |
|-------|----------------------------|
|0x0    |  MCU_STATUS                |
|0x80   |  DOORBELL_IRQ_STATUS       |
|0x180  |  DOORBELL_IRQ_CLEAR        |
|0x200  |  DOORBELL_IRQ_MASK   (RO)  |
|0x280  |  DOORBELL_IRQ_ENABLE (WO)  |

## Timers

Base: 0x40024000

|Offset |  Name                      |
|-------|----------------------------|
|0x0    |  TIMER0_ENABLE             |
|0x4    |  TIMER1_ENABLE             |
|0x10   |  TIMER0_COUNTER_LOW        |
|0x14   |  TIMER0_COUNTER_HIGH       |
|0x18   |  TIMER0_DURATION           |
|0x30   |  TIMER1_COUNTER_LOW        |
|0x34   |  TIMER1_COUNTER_HIGH       |
|0x38   |  TIMER1_DURATION           |

## Iterators

Compute iterator $i$ base: 0x40031000 + 0x100 * i

Fragment iterator $i$ base: 0x40032000 + 0x100 * i

Tiler iterator $i$ base: 0x40033000 + 0x100 * i

|Offset  | Name                         |
|------  | ----                         |
|0x00    | ITER_CTRL                    |
|0x04    | ITER_STATUS                  |
|0x08    | ITER_CONFIG                  |
|0x10    | ITER_ENDPOINT_ALLOW_LO       |
|0x14    | ITER_ENDPOINT_ALLOW_HI       |
|0x18    | ITER_ENDPOINT_READY_LO       |
|0x1c    | ITER_ENDPOINT_READY_HI       |
|0x20    | ITER_QUEUE_COUNT             |
|0xa0    | ITER_BLOCKED_SB_ENTRY        |
|0xa4    | ITER_ENDPOINT_EVENT          |
|0xc0    | ITER_SHADER_DOORBELL_RAW     |
|0xc4    | ITER_SHADER_DOORBELL_MASK    |
|0xcc    | ITER_SHADER_DOORBELL_STATUS  |
|0xd0    | ITER_IRQ_RAW                 |
|0xd4    | ITER_IRQ_MASK                |
|0xd8    | ITER_IRQ_CLEAR               |
|0xdc    | ITER_IRQ_STATUS              |
|0xe0    | ITER_FAULT_STATUS            |
|0xe4    | ITER_FAULT_SCOREBOARD        |
|0xe8    | ITER_FAULT_ADDRESS_LO        |
|0xec    | ITER_FAULT_ADDRESS_HI        |

## CSHWIF

Base for CSHWIF $i$: 0x40034000 + 0x100 * i

|Offset |  Name                      |
|------ |  ----                      |
|0x00   |  CSHWIF_CMD_PTR_LO         |
|0x04   |  CSHWIF_CMD_PTR_HI         |
|0x08   |  CSHWIF_CMD_PTR_END_LO     |
|0x0c   |  CSHWIF_CMD_PTR_END_HI     |
|0x18   |  CSHWIF_KICK               |
|0x1c   |  CSHWIF_KICKED             |
|0x20   |  CSHWIF_CTRL               |
|0x24   |  CSHWIF_STATUS             |
|0x28   |  CSHWIF_ITER_COMPUTE       |
|0x2c   |  CSHWIF_ITER_FRAGMENT      |
|0x30   |  CSHWIF_ITER_TILER         |
|0x34   |  CSHWIF_JASID              |
|0x40   |  CSHWIF_TRAP_CFG_0_LO      |
|0x44   |  CSHWIF_TRAP_CFG_0_HI      |
|0x48   |  CSHWIF_TRAP_CFG_1_LO      |
|0x4c   |  CSHWIF_TRAP_CFG_1_HI      |
|0x50   |  CSHWIF_TRAP_CFG_2_LO      |
|0x54   |  CSHWIF_TRAP_CFG_2_HI      |
|0x58   |  CSHWIF_TRAP_CFG_3_LO      |
|0x5c   |  CSHWIF_TRAP_CFG_3_HI      |
|0x60   |  CSHWIF_EMULATION_INSTR_LO |
|0x64   |  CSHWIF_EMULATION_INSTR_LO |
|0x74   |  CSHWIF_WAIT_STATUS        |
|0x78   |  CSHWIF_SB_SET_SEL         |
|0x7c   |  CSHWIF_SB_SEL             |
|0x98   |  CSHWIF_EVENT_RAW          |
|0xa0   |  CSHWIF_EVENT_IRQ_ENABLE   |
|0xa4   |  CSHWIF_EVENT_IRQ_STATUS   |
|0xa8   |  CSHWIF_EVENT_HALT_ENABLE  |
|0xac   |  CSHWIF_EVENT_HALT_STATUS  |
|0xb0   |  CSHWIF_FAULT_STATUS       |
|0xb8   |  CSHWIF_FAULT_ADDRESS_LO   |
|0xbc   |  CSHWIF_FAULT_ADDRESS_HI   |

To submit work to the CSHWIF, first kick it (write a 1 to KICK and wait until KICKED becomes 1), then write out the command pointer start and end.

## CSHWIF register file

Each CSHWIF $i$ has $n$ 32-bit registers. The count $n$ is given in the lower half of `0x4030010 + i * 4`. Register $r$ is at address `0x40038000 + 0x400 * i + 4 * r`.

## Scoreboard

Base: 0x40036000

|Offset         |  Name                                                                           |
|------         |  ----                                                                           |
|0x0            |  SB_ZERO_INC (set selected through SB_SET_SEL, each bit represents an SB_VALUE) |
|0x4            |  SB_ZERO_DEC (set selected through SB_SET_SEL, each bit represents an SB_VALUE) |
|0x8            |  SB_ZERO_MASK (set selected through SB_SET_SEL)                                 |
|0xc            |  SB_ZERO (set selected through SB_SET_SEL)                                      |
|0x80           |  SB_SET_SEL                                                                     |
|[0xa0:0xc0[    |  SB_ALL_ZERO_IRQ_RAWSTAT                                                        |
|[0xc0:0xe0[    |  SB_ALL_ZERO_IRQ_MASK (1 bit per set)                                           |
|[0xe0:0x100[   |  SB_ALL_ZERO_IRQ_STATUS (1 bit per set)                                         |
|[0x100:0x120[  |  SB_ZERO_MASK_IRQ_RAWSTAT                                                       |
|[0x120:0140[   |  SB_ZERO_MASK_IRQ_MASK (1 bit per set)                                          |
|[0x140:0160[   |  SB_ZERO_MASK_IRQ_STATUS (1 bit per set)                                        |
|[0x160:0x180[  |  SB_FAULT_IRQ_RAWSTAT                                                           |
|[0x180:0x1a0[  |  SB_FAULT_IRQ_MASK (1 bit per set)                                              |
|[0x1a0:0x1c0[  |  SB_FAULT_IRQ_STATUS (1 bit per set)                                            |
|[0x1c0:0x200[  |  SB_FAULT_IRQ_CLEAR (1 bit per set)                                             |
